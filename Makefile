
TEX = luatex --file-line-error --shell-escape
EUPL = EUPL-1.2-EN.txt

XMP_DIR = xmp
XMP_FILES = minim-xmp.tex minim-xmp.lua minim-xmp.doc
PDF_DIR = pdf
PDF_FILES = minim-pdf.tex minim-pdf.lua minim-languagecodes.lua minim-pdf.doc
MATHS_DIR = mathematics
MATHS_FILES = minim-math.tex minim-math.lua minim-math-table.lua minim-math.doc
HATCH_DIR = hatching
HATCH_FILES = minim-hatching.mp minim-hatching-doc.mp
MP_DIR = metapost
MP_FILES = minim.mp minim-mp.mp minim-mp.ini minim-lamp.ini minim-lamp.mp minim-mp.tex minim-mp.lua minim-mp.doc minim-mp.sty
MAIN_DIR = minim
MAIN_FILES = minim-etex.tex minim-plain.tex minim-lmodern.tex minim.tex minim.ini minim.doc \
	minim-alloc.tex minim-alloc.lua minim-hooks.tex minim-hooks.lua minim-callbacks.lua \
	minim-doc.sty minim-alloc.doc minim-pdfresources.lua minim-pdfresources.tex

documentation : formats \
	xmp/minim-xmp.pdf \
	pdf/minim-pdf.pdf \
	mathematics/minim-math.pdf \
	metapost/minim-mp.pdf \
	hatching/minim-hatching-doc.pdf \
	minim/minim.pdf
	echo "All manuals made."

packages : documentation \
	minim-xmp.tar.gz \
	minim-pdf.tar.gz \
	minim-math.tar.gz \
	minim-mp.tar.gz \
	minim-hatching.tar.gz \
	minim.tar.gz
	@./check-versions

formats : minim/minim.fmt metapost/minim-mp.fmt metapost/minim-lamp.fmt
minim/minim.fmt : */*.tex */*.lua minim/minim.ini
	cd ${MAIN_DIR}; luatex -ini minim.ini; mv minim.log minim-fmt.log
metapost/minim-mp.fmt : minim/minim.fmt metapost/minim-mp.ini
	cd ${MP_DIR}; luatex -ini minim-mp.ini; mv minim-mp.log minim-mp-fmt.log
metapost/minim-lamp.fmt : minim/minim.fmt metapost/minim-lamp.ini
	cd ${MP_DIR}; luatex -ini minim-lamp.ini; mv minim-lamp.log minim-lamp-fmt.log

clean:
	rm -f *.tar.gz *.log *.aux */*.log */*.aux

xmp/minim-xmp.pdf : $(addprefix ${XMP_DIR}/,${XMP_FILES}) minim/minim-doc.sty
	cd ${XMP_DIR}; ${TEX} minim-xmp.doc
minim-xmp.tar.gz : xmp/minim-xmp.pdf
	tar czf minim-xmp.tar.gz --transform='s/^LICENSE/minim-xmp\/${EUPL}/;s/^[^\/]*/minim-xmp/' \
		$(addprefix ${XMP_DIR}/,${XMP_FILES}) LICENSE xmp/README xmp/minim-xmp.pdf

pdf/minim-pdf.pdf : $(addprefix ${PDF_DIR}/,${PDF_FILES}) minim/minim-doc.sty minim/minim.fmt
	cd ${PDF_DIR}; ${TEX} minim-pdf.doc
minim-pdf.tar.gz : pdf/minim-pdf.pdf
	tar czf minim-pdf.tar.gz --transform='s/^LICENSE/minim-pdf\/${EUPL}/;s/^[^\/]*/minim-pdf/' \
		$(addprefix ${PDF_DIR}/,${PDF_FILES}) LICENSE pdf/README pdf/minim-pdf.pdf

mathematics/minim-math.pdf : $(addprefix ${MATHS_DIR}/,${MATHS_FILES}) minim/minim-doc.sty
	cd ${MATHS_DIR}; ${TEX} minim-math.doc
minim-math.tar.gz : mathematics/minim-math.pdf
	tar czf minim-math.tar.gz --transform='s/^LICENSE/minim-math\/${EUPL}/;s/^[^\/]*/minim-math/' \
		$(addprefix ${MATHS_DIR}/,${MATHS_FILES}) LICENSE mathematics/README mathematics/minim-math.pdf

metapost/minim-mp.pdf : $(addprefix ${MP_DIR}/,${MP_FILES}) minim/minim-doc.sty metapost/minim-mp.fmt
	cd ${MP_DIR}; ${TEX} minim-mp.doc
minim-mp.tar.gz : metapost/minim-mp.pdf
	tar czf minim-mp.tar.gz --transform='s/^LICENSE/minim-mp\/${EUPL}/;s/^[^\/]*/minim-mp/' \
		$(addprefix ${MP_DIR}/,${MP_FILES}) LICENSE metapost/README metapost/minim-mp.pdf

hatching/minim-hatching-doc.pdf : $(addprefix ${HATCH_DIR}/,${HATCH_FILES}) minim/minim-doc.sty metapost/minim-mp.fmt
	cd ${HATCH_DIR}; ${TEX} --fmt=minim-mp minim-hatching-doc.mp
minim-hatching.tar.gz : hatching/minim-hatching-doc.pdf
	tar czf minim-hatching.tar.gz --transform='s/^LICENSE/minim-hatching\/${EUPL}/;s/^[^\/]*/minim-hatching/' \
		$(addprefix ${HATCH_DIR}/,${HATCH_FILES}) LICENSE hatching/README hatching/minim-hatching-doc.pdf

minim/minim.pdf : $(addprefix ${MAIN_DIR}/,${MAIN_FILES}) */*.doc minim/minim-doc.sty
	cd ${MAIN_DIR}; ${TEX} minim.doc
	cd ${MAIN_DIR}; ${TEX} --fmt=minim minim.doc
	pdfinfo -struct minim/minim.pdf 2>/dev/null > minim/minim.struct
minim.tar.gz : minim/minim.pdf
	tar czf minim.tar.gz --transform='s/^LICENSE/minim\/${EUPL}/;s/^[^\/]*/minim/' \
		$(addprefix ${MAIN_DIR}/,${MAIN_FILES}) LICENSE minim/README minim/minim.pdf

TEXMFHOME ?= ~/texmf
LOCALMP = ${TEXMFHOME}/metapost/minim
LOCALTEX = ${TEXMFHOME}/luatex/minim
LOCALWEB2C = ${TEXMFHOME}/web2c/luatex
INSTALL :
	mkdir -p ${LOCALMP} ${LOCALTEX} ${LOCALWEB2C}
	cp */*.mp ${LOCALMP}
	cp */*.{tex,lua,doc,ini} ${LOCALTEX}
	cd ${LOCALWEB2C}; luatex -ini minim.ini
	cd ${LOCALWEB2C}; luatex -ini minim-mp.ini
	cd ${LOCALWEB2C}; luatex -ini minim-lamp.ini

